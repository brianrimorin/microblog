<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\FollowController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\PostImageController;
use App\Http\Controllers\UserImageController;
use App\Http\Controllers\Auth\RegisteredUserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::permanentRedirect('/', '/login');

Route::resource('/user', RegisteredUserController::class)
    ->except(['show', 'index', 'create', 'update'])
    ->middleware(['verified']);

Route::put('/user/{user:username}', [RegisteredUserController::class, 'update'])
    ->middleware(['verified']);

Route::get('/user/{user:username}', [RegisteredUserController::class, 'show'])
    ->middleware(['verified']);

//Search
Route::get('/search', [RegisteredUserController::class, 'index'])
    ->middleware(['auth'])
    ->name('search');

//Following
Route::get('/user/{user:username}/following', [RegisteredUserController::class, 'listFollowing'])
    ->middleware(['verified']);

//Followers
Route::get('/user/{user:username}/followers', [RegisteredUserController::class, 'listFollowers'])
    ->middleware(['verified']);

//Follow/ Unfollow
Route::post('/user/{user:username}/follow', [FollowController::class, 'store'])
    ->middleware(['verified']);

Route::delete('/user/{user:username}/unfollow', [FollowController::class, 'destroy'])
    ->middleware(['verified']);

//Post CRUD
Route::resource('/post', PostController::class)
    ->except(['index'])
    ->middleware(['verified']);

//Feed
Route::get('/feed', [PostController::class, 'index'])
    ->middleware(['verified'])
    ->name('feed');

//Share
Route::get('/post/{post}/share', [PostController::class, 'share'])
->middleware(['verified']);

//Comment CRUD
Route::resource('/comment', CommentController::class)
    ->except(['create', 'index', 'show'])
    ->middleware(['verified']);

//Comment List
Route::get('/user/{user:username}/comments', [CommentController::class, 'index'])
    ->middleware(['verified']);

//Like
Route::post('/post/{post}/like', [LikeController::class, 'store'])
    ->middleware(['verified']);

//Like
Route::delete('/post/{post}/unlike', [LikeController::class, 'destroy'])
->middleware(['verified']);

//Appears to be the new counterpart to Auth::routes();
require __DIR__ . '/auth.php';

//Post Image
Route::get('/post/{post}/image', PostImageController::class)
    ->middleware(['verified']);

//User Image
Route::get('/user/{user:username}/image', UserImageController::class)
    ->middleware(['verified']);
