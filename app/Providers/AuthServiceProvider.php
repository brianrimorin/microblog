<?php

namespace App\Providers;

use App\Models\Post;
use App\Models\User;
use App\Models\Comment;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('modify-post', function (User $user, Post $post) {
            return $user->id === $post->user->id;
        });

        Gate::define('modify-comment', function (User $user, Comment $comment) {
            return $user->id === $comment->user->id;
        });

        Gate::define('modify-user', function (User $user, $user2) {
            return $user->id === $user2->id;
        });

    }
}
