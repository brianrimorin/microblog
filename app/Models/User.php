<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;
    use SoftDeletes;
    use Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'email',
        'password',
        'image_path',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts(bool $paginated = false)
    {
        if ($paginated) {
            return $this->hasMany(Post::class)
                ->orderByDesc("created_at")
                ->paginate(config('constants.page_size'));
        }

        return $this->hasMany(Post::class);
    }

    public function follows(bool $paginated = false)
    {
        if ($paginated) {
            return $this->belongsToMany(User::class, 'follows', 'user_id', 'followed_user_id')
                ->paginate(config('constants.page_size'));
        }

        return $this->belongsToMany(User::class, 'follows', 'user_id', 'followed_user_id');
    }

    public function followers(bool $paginated = false)
    {
        if ($paginated) {
            return $this->belongsToMany(User::class, 'follows', 'followed_user_id', 'user_id')
                ->paginate(config('constants.page_size'));
        }

        return $this->belongsToMany(User::class, 'follows', 'followed_user_id', 'user_id');
    }

    public function comments(bool $paginated = false)
    {
        if ($paginated) {
            return $this->hasMany(Comment::class)
                ->orderByDesc("created_at")
                ->paginate(config('constants.page_size'));
        }

        return $this->hasMany(Comment::class);
    }

    public function likes(bool $paginated = false)
    {
        if ($paginated) {
            return $this->belongsToMany(Post::class, 'likes', 'user_id', 'post_id')
                ->paginate(config('constants.page_size'));
        }

        return $this->belongsToMany(Post::class, 'likes', 'user_id', 'post_id');
    }

    public function shouldBeSearchable()
    {
        return $this->hasVerifiedEmail();
    }

    public function deleteActivity()
    {
        foreach ($this->posts as $post) {
            $post->delete();
        }

        foreach ($this->comments as $comment) {
            $comment->delete();
        }

        $this->delete();
    }

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
