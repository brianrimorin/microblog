<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'original_post_id',
        'content',
        'image_path',
        'created_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments(bool $paginated = false)
    {
        if ($paginated) {
            return $this->hasMany(Comment::class)
                ->orderByDesc("created_at")
                ->paginate(config('constants.page_size'));
        }

        return $this->hasMany(Comment::class);
    }

    public function originalPost()
    {
        return $this->belongsTo(Post::class, 'original_post_id');
    }

    public static function postsFromFollowing(User $user)
    {
        $followIds = $user->follows->pluck('id');

        $posts = Post::whereIn('user_id', $followIds)
            ->orderByDesc("created_at")
            ->paginate(config('constants.page_size'));

        return $posts;
    }

    public function createdAtRelative()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    public function updatedAtRelative()
    {
        return Carbon::parse($this->updated_at)->diffForHumans();
    }

    // public function likeCount()
    // {
    //     return Like::where('post_id', $this->id)->count();
    // }

    public function likeCount()
    {
        $count = $this->belongsToMany(Post::class, 'likes', 'post_id', 'user_id')->count();
        if ($count == 0) {
            return;
        }

        if ($count == 1) {
            return "1 Like";
        }

        return $count . ' Likes';
    }
}
