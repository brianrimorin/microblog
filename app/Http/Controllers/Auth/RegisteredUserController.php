<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use App\Providers\RouteServiceProvider;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'first_name' => ['required', 'string', 'max:255', 'regex:/^[\pL\s\-]+$/u'],
            'last_name' => ['required', 'string', 'max:255', 'regex:/^[\pL\s\-]+$/u'],
            'username' => ['required', 'string', 'max:255',
                            'alpha_dash', 'unique:users',
                            'regex:/^[a-zA-Z]{1}/',],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'image' => ['image', 'max:5000', 'nullable'],
        ]);

        if (Arr::has($validated, 'image')) {
            $filename = $request->file('image')->store('user_images');
            $filename = str_replace('user_images/', '', $filename);
            $validated['image_path'] = $filename;
        }

        $validated['password'] = Hash::make($request->password);
        $user = User::create($validated);

        event(new Registered($user));
        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }

    public function show(User $user)
    {
        if (!$user->hasVerifiedEmail()) {
            abort(403);
        }

        return view('user.show', [
            'user' => $user,
            'posts' => $user->posts(true),
            'title' => $user->fullName() . '\'s Profile',
        ]);
    }

    public function edit(User $user)
    {
        if (! Gate::allows('modify-user', $user)) {
            abort(403);
        }

        return view('user.edit', [
            'user' => $user,
        ]);
    }

    public function update(Request $request, User $user)
    {
        if (! Gate::allows('modify-user', $user)) {
            abort(403);
        }

        $validated = $request->validate([
            'first_name' => ['required', 'string', 'max:255', 'regex:/^[\pL\s\-]+$/u'],
            'last_name' => ['required', 'string', 'max:255', 'regex:/^[\pL\s\-]+$/u'],
            'image' => ['image', 'max:5000', 'nullable'],
        ]);

        if (Arr::has($validated, 'image')) {
            $filename = $request->file('image')->store('user_images');
            $filename = str_replace('user_images/', '', $filename);
            $validated['image_path'] = $filename;
        }

        $user->update($validated);

        return redirect('/user/' . $user->username);
    }

    public function destroy()
    {
        Auth::user()->deleteActivity; 

        return redirect('/feed');
    }

    public function index(Request $request)
    {

        $validated = $request->validate([
            'term' => ['required', 'max:255'],
        ]);

        $users = User::search($validated['term'])
            ->paginate(config('constants.page_size'));

        return view('user.index', [
            'users' => $users,
            'title' => 'Search Results',
        ]);
    }

    public function listFollowers(User $user)
    {
        return view('user.index', [
            'users' => $user->followers(true),
            'title' => $user->fullName() . '\'s Followers',
        ]);
    }

    public function listFollowing(User $user)
    {
        return view('user.index', [
            'users' => $user->follows(true),
            'title' => $user->fullName() . '\'s Followed Users',
        ]);
    }
}
