<?php

namespace App\Http\Controllers;

use App\Models\User;

class UserImageController extends Controller
{
    public function __invoke(User $user)
    {
        if (is_null($user->image_path)) {
            // return response()->file(storage_path('app/user_images/default.png'));
            return response()->file(public_path('default.png'));
        }

        return response()->file(storage_path('app/user_images/' . $user->image_path));
    }
}
