<?php

namespace App\Http\Controllers;

use App\Models\Post;

class PostImageController extends Controller
{
    public function __invoke(Post $post)
    {
        return response()->file(storage_path('app/post_images/' . $post->image_path));
    }
}
