<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::postsFromFollowing(Auth::user());

        return view('feed', [
            'posts' => $posts,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'content' => ['required', 'string', 'max:140'],
            //Not filtering by specific mimetypes for now
            'image' => ['image', 'max:5000', 'nullable'],
            'original_post_id' => ['integer', 'nullable'],
        ]);

        if (Arr::has($validated, 'image')) {
            $filename = $request->file('image')->store('post_images');
            $filename = str_replace('post_images/', '', $filename);
            $validated['image_path'] = $filename;
        }

        $validated['user_id'] = Auth::id();
        Post::create($validated);

        return redirect('/feed')
            ->with('status', 'Post successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    // public function show(Post $post)
    public function show(Post $post)
    {
        $comments = $post->comments(true);

        return view('post.show', [
            'post' => $post,
            'comments' => $comments
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        if (! Gate::allows('modify-post', $post)) {
            abort(403);
        }

        return view('post.edit', [
            'post' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        if (! Gate::allows('modify-post', $post)) {
            abort(403);
        }

        $validated = $request->validate([
            'content' => ['required', 'string', 'max:140'],
        ]);

        $post->update($validated);

        return redirect('/post/' . $post->id)
            ->with('status', 'Post successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if (! Gate::allows('modify-post', $post)) {
            abort(403);
        }

        foreach ($post->comments as $comment) {
            $comment->delete();
        }

        $post->delete();

        return redirect('/feed')
            ->with('status', 'Post successfully deleted!');
    }

    public function share(Post $post)
    {
        return view('post.share', [
            'post' => $post
        ]);
    }
}
