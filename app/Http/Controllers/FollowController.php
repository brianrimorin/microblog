<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Follow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FollowController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $user)
    {
        if (Auth::id() == $user->id) {
            return back();
        }

        Follow::firstOrCreate([
            'user_id' => Auth::id(),
            'followed_user_id' => $user->id,
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Follow  $follow
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        Follow::where('user_id', Auth::id())
            ->where('followed_user_id', $user->id)
            ->delete();

        return back();
    }
}
