<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'content' => ['required', 'string', 'max:140'],
        ]);

        $validated['user_id'] = Auth::id();
        $validated['post_id'] = $request->post_id;
        Comment::create($validated);

        return redirect('/post/' . $request->post_id)
            ->with('status', 'Comment successfully created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        if (! Gate::allows('modify-comment', $comment)) {
            abort(403);
        }

        return view('comment.edit', [
            'comment' => $comment
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        if (! Gate::allows('modify-comment', $comment)) {
            abort(403);
        }

        $validated = $request->validate([
            'content' => ['required', 'string', 'max:140'],
        ]);

        $comment->update($validated);

        return redirect('/post/' . $comment->post_id)
            ->with('status', 'Comment successfully edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        if (! Gate::allows('modify-comment', $comment)) {
            abort(403);
        }

        $comment->delete();

        return back()
            ->with('status', 'Comment successfully deleted!');
    }

    public function index(User $user)
    {
        return view('comment.index', [
            'comments' => $user->comments(true),
            'title' => $user->fullName() . '\'s Comments',
        ]);
    }
}
