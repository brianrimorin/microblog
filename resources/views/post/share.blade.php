<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Share Post') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <!-- Validation Errors -->
                    <x-validation-errors class="mb-4" :errors="$errors" />

                    <form method="POST" action="{{ route('post.store') }}" >
                        @csrf

                        <div class="mt-4">
                            <x-label for="content" :value="__('Add Comment to Shared Post')" />
                            <x-input id="content" class="block mt-1 w-full"  name="content"  />
                        </div>

                        @isset($post->original_post_id)
                            <x-input id="original_post_id" type="hidden" name="original_post_id" :value="$post->original_post_id"/>
                        @else   
                            <x-input id="original_post_id" type="hidden" name="original_post_id" :value="$post->id"/>
                        @endisset
                        
                        <x-button>Create Post</x-button>
                    </form>
                </div> 

                <div class="p-6 bg-white border-b border-gray-200">
                    @include('layouts.post')
                </div>
            </div>
        </div>
    </div>
</x-app-layout>