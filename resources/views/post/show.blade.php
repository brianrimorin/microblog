<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Post') }}
        </h2>
    </x-slot>
    
    @include('layouts.alert')

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg p-6 border-b border-gray-200">
                @include('layouts.post')
                @include('layouts.post-buttons')
            </div>
        </div>
    </div>
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        @if (Auth::user()->follows->contains($post->user) 
            || Auth::id() == $post->user->id)
            Comments:
            <div class="p-6 bg-white border-b border-gray-200">
                <!-- Validation Errors -->
                <x-validation-errors class="mb-4" :errors="$errors" />
                <form method="POST" action="{{ route('comment.store') }}" enctype="multipart/form-data">
                    @csrf
        
                    <!-- Comment Field -->
                    <div class="mt-4">
                        <x-label for="content" :value="__('Write a Comment')" />
                        <x-input id="post_id" type="hidden" name="post_id" :value="$post->id"/>
                        <x-input id="content" class="block mt-1 w-full"  name="content" :value="old('content')"  />
                    </div>
                    
                    <div class="flex items-center justify-end mt-4">
                        {{-- <x-button class="ml-4">
                            {{ __('Submit Comment') }}
                        </x-button> --}}
                        <x-button>Submit Comment</x-button>
                    </div>
                </form>
            </div>
            
            @forelse ($comments as $comment)
                <div class="p-6 bg-white border-b border-gray-200">
                    @include('layouts.comment')
                    @include('layouts.comment-buttons')
                </div>
            @empty
                No comments available.
            @endforelse
            {{ $comments->links() }}
        @else
            <div class="p-6 bg-white border-b border-gray-200">
                Comments cannot be viewed. Follow post author first.
            </div>
        @endif
    </div>

</x-app-layout>