@error('term')
    <div class="font-medium text-red-600">
        {{ __('Whoops! Something went wrong.') }}
    </div>

    <ul class="mt-3 list-disc list-inside text-sm text-red-600">
        <li>{{ $message }}</li>
    </ul>
@enderror

<form method="GET" action="{{ route('search') }}" >
    <!-- Search Bar -->
    <div class="mt-4">
        <x-label for="term" :value="__('Search')" />

        <x-input id="term" class="block mt-1 w-full"  name="term" />
    </div>

    <div class="flex items-center justify-end mt-4">
        <x-button>Search</x-button>
    </div>
</form>