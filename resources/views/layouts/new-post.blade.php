@error('content')
    <div class="font-medium text-red-600">
        {{ __('Whoops! Something went wrong.') }}
    </div>

    <ul class="mt-3 list-disc list-inside text-sm text-red-600">
        <li>{{ $message }}</li>
    </ul>
@enderror

<form method="POST" action="{{ route('post.store') }}" enctype="multipart/form-data">
    @csrf

    <!-- Post Content -->
    <div class="mt-4">
        <x-label for="content" :value="__('Post')" />

        <x-input id="content" class="block mt-1 w-full"  name="content" :value="old('content')"  />
    </div>

    <!-- Post Attachment -->
    <div class="mt-4">
        <x-label for="image" :value="__('Attach an image (Optional)')" />

        <x-label :value="__('Supported formats: jpg, jpeg, png, bmp, gif, svg, webp')"
        class="block"/>    

        <x-label :value="__('Maximum size: 5 MB')" class="block"/>   

        <x-input id="image" class="block mt-1 w-full"
                        type="file"
                        name="image"
                        onchange="previewFile()"/>
    </div>

    <div>
        <img id="preview" width="100">
    </div>
    
    <div class="flex items-center justify-end mt-4">
        <x-button>Submit Post</x-button>
    </div>
</form>

<script>
    // template taken from readAsDataURL example
    function previewFile() {
        const preview = document.getElementById('preview');
        const file = document.querySelector('input[type=file]').files[0];
        const reader = new FileReader();

        reader.addEventListener("load", function () {
            preview.src = reader.result;
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }
    }
</script>