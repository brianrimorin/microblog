<div class="flex flex-row">
    <div>
        <a href="/user/{{ $comment->user->username }}">
            <img src="/user/{{ $comment->user->username }}/image" width="50">
        </a>
    </div>

    <div class="flex-grow pl-6">
        <div>
            <a href="/user/{{ $comment->user->username }}">
                {{ $comment->user->fullName() }}
            </a>
            
            <x-label :value="'· ' . $comment->user->username" />
            ·
            <x-label :value="$comment->createdAtRelative()" />

            @if ($comment->created_at != $comment->updated_at)
                <x-label :value="'(Edited ' . $comment->updatedAtRelative() . ')'" />
            @endif
        </div>

        <div class="block">
            {{ $comment->content }}
        </div>
    </div>
</div>