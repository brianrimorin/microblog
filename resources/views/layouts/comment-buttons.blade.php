@if (Auth::id() == $comment->user_id)
    <div class="flex flex-row">
        <x-button type="button" onclick="document.location='/comment/{{ $comment->id }}/edit'">
            Edit Comment
        </x-button>

        <form action="/comment/{{ $comment->id }}" method="POST" 
            onsubmit="return confirm('Delete this comment?');">
            @csrf
            @method('delete')

            <x-button>Delete Comment</x-button>
        </form>
    </div>
@endif
