<div class="flex flex-row">
    @if (Auth::id() == $post->user_id)
        <div>
            <x-button type="button" onclick="document.location='/post/{{ $post->id }}/edit'">
                Edit Post
            </x-button>
        </div>

        <div>
            <form action="/post/{{ $post->id }}" method="POST"
                onsubmit="return confirm('Delete this post?');">
                @csrf
                @method('delete')

                <x-button>Delete Post</x-button>
            </form>
        </div>
    @endif

    <div>
        <x-button type="button" onclick="document.location='/post/{{ $post->id }}/share'">
            Share
        </x-button>
    
    </div>

    <div> 
        @if (Auth::user()->likes->contains($post) )
            <form method="POST" action="/post/{{ $post->id }}/unlike" >
                @csrf
                @method('delete')
                
                <x-button>Liked</x-button>
            </form>
            
        @else
            <form method="POST" action="/post/{{ $post->id }}/like" >
                @csrf

                <x-button>Like</x-button>
            </form>
        
        @endif
    </div>

    @if(!request()->is('post/' . $post->id))
        <div>
            <x-button type="button" onclick="document.location='/post/{{ $post->id }}'">
                Show Comments
            </x-button>
        
        </div>
    @endif
</div>