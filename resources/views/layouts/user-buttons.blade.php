@if (Auth::user()->follows->contains($user) )
    <form method="POST" action="/user/{{ $user->username }}/unfollow" >
        @csrf
        @method('delete')

        <x-button>Unfollow</x-button>
    </form>
@else
    <form method="POST" action="/user/{{ $user->username }}/follow" >
        @csrf

        <x-button>Follow</x-button>
    </form>
@endif