<div class="flex flex-row">
    <div>
        <a href="/user/{{ $post->user->username }}">
            <img src="/user/{{ $post->user->username }}/image" width="50">
        </a>
    </div>
    
    <div class="flex-grow pl-6">
        <div>
            <a href="/user/{{ $post->user->username }}">
                {{ $post->user->fullName() }}
            </a>
            
            <x-label :value="'· ' . $post->user->username" />
            ·
            <x-label :value="$post->createdAtRelative()" />
                
            @if ($post->created_at != $post->updated_at)
                <x-label :value="'(Edited ' . $post->updatedAtRelative() . ')'" />
            @endif
        </div>
        
        @if (Auth::user()->follows->contains($post->user) 
        || Auth::user()->id == $post->user->id)
            <div>
                <a href="/post/{{ $post->id }}">
                    {{ $post->content }}
                </a>
            </div>

            @isset($post->image_path)
                <div>
                    <img 
                        src="/post/{{ $post->id }}/image"
                        width="200"
                    />
                </div>
            @endisset

            {{ $post->likeCount() }}

            @isset($post->original_post_id)
                <div class="p-6 bg-white border border-gray-200">
                    @isset($post->originalPost)
                        @include('layouts.post', $post = $post->originalPost)
                    @else 
                        Content not found.
                    @endisset
                </div>
            @endisset
        @else
            Post cannot be viewed. Follow post author first.
        @endif

        
    </div>
</div>