<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __($title) }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg grid-rows-1">
                <div class="p-6 bg-white border-b border-gray-200">
                    <x-button type="button" onclick="document.location='/'">
                        Go Back
                    </x-button>
                </div>

                <div class="p-6 bg-white border-b border-gray-200">
                    @forelse ($comments as $comment)
                        <div class="p-6 bg-white border-b border-gray-200">
                            @include('layouts.post', $post = $comment->post)
                        
                            <div class="p-6 bg-white border border-gray-200">
                                @include('layouts.comment')
                                @include('layouts.comment-buttons')
                            </div>
                        </div>
                    @empty
                        No comments available.
                    @endforelse
                </div>
            </div>
            {{ $comments->links() }}
        </div>
    </div>
</x-app-layout>