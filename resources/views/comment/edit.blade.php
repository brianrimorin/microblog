<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Comment') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <!-- Validation Errors -->
                    <x-validation-errors class="mb-4" :errors="$errors" />

                    <form action="/comment/{{ $comment->id }}" method="POST">
                        @csrf
                        @method('PUT')
                        
                        <div class="mt-4">
                            <x-label for="content" :value="__('Comment')" />
            
                            <x-input id="content" class="block mt-1 w-full"  name="content" :value="$comment->content"  />
                        </div>

                        <x-button>Submit</x-button>
                    </form>
                </div> 
            </div>
        </div>
    </div>
</x-app-layout>
