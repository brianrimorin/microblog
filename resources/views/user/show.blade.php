<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __($title) }}
        </h2>
    </x-slot>
    
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg grid-rows-1">
                <div class="p-6 bg-white border-b border-gray-200">
                    @include('layouts.user')
                    
                    <div class="flex items-center mt-4">
                        @if (Auth::id() == $user->id)
                            <x-button type="button" onclick="document.location='/user/{{ Auth::id() }}/edit'">
                                Edit Profile
                            </x-button>
                        @else  
                            @include('layouts.user-buttons')
                        @endif

                        <x-button type="button" onclick="document.location='/user/{{ $user->username }}/followers'">
                            Followers
                        </x-button>

                        <x-button type="button" onclick="document.location='/user/{{ $user->username }}/following'">
                            Following
                        </x-button>

                        <x-button type="button" onclick="document.location='#posts'">
                            Posts
                        </x-button>

                        <x-button type="button" onclick="document.location='/user/{{ $user->username }}/comments'">
                            Comments
                        </x-button>
                    </div>
                </div>

                <div class="p-6 bg-white border-b border-gray-200">
                    <p class="text-xl" id="posts">
                        Posts:
                    </p>
                </div>
                @forelse ($posts as $post) 
                    <div class="p-6 bg-white border-b border-gray-200">
                        @include('layouts.post')
                        @include('layouts.post-buttons')
                    </div>
                @empty
                    No posts available.
                @endforelse

                <div class="p-6 bg-white border-b border-gray-200">
                    {{ $posts->links() }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
