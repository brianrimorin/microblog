<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __($title) }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg grid-rows-1">
                <div class="p-6 bg-white border-b border-gray-200">
                    <x-button type="button" onclick="document.location='/'">
                        Go Back
                    </x-button>
                </div>

                <div class="p-6 bg-white border-b border-gray-200">
                    @forelse ($users as $user)
                        @include('layouts.user')
                        @if ($user->id != Auth::id())
                            @include('layouts.user-buttons')
                        @endif
                    @empty
                        No results available.
                    @endforelse
                </div>
            </div>
            {{ $users->links() }}
        </div>
    </div>
</x-app-layout>