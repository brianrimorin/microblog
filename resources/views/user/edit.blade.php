<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit User Details') }}
        </h2>
    </x-slot>
    
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <!-- Validation Errors -->
                    <x-validation-errors class="mb-4" :errors="$errors" />

                    <form action="/user/{{ Auth::user()->username }}" method="POST" 
                        enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        
                        <!-- First Name -->
                        <div class="mt-4">
                            <x-label for="first_name" :value="__('First Name')" />
            
                            <x-input id="first_name" class="block mt-1 w-full"  name="first_name" :value="$user->first_name"  />
                        </div>

                        <!-- Last Name -->
                        <div class="mt-4">
                            <x-label for="last_name" :value="__('Last Name')" />
            
                            <x-input id="last_name" class="block mt-1 w-full"  name="last_name" :value="$user->last_name"  />
                        </div>

                        <!-- Email Address -->
                        {{-- <div class="mt-4">
                            <x-label for="email" :value="__('Email Address')" />
            
                            <x-input id="email" class="block mt-1 w-full"  name="email" :value="$user->email"  />
                        </div> --}}

                        <!-- Profile Picture -->
                        <div class="mt-4">
                            <x-label for="image" :value="__('Profile Picture')" />

                            <x-label :value="__('Supported formats: jpg, jpeg, png, bmp, gif, svg, webp')"
                            class="block"/>    
        
                            <x-label :value="__('Maximum size: 5 MB')" class="block"/>   
                            
                            <div>
                                <img id="preview" src="/user/{{ $user->username }}/image" width="100">
                            </div>

                            <x-input id="image" class="block mt-1 w-full"
                                            type="file"
                                            name="image"
                                            onchange="previewFile()"/>
                        </div>

                        <x-button>Submit</x-button>
                    </form>

                    <script>
                        // template taken from readAsDataURL example
                        function previewFile() {
                            const preview = document.getElementById('preview');
                            const file = document.querySelector('input[type=file]').files[0];
                            const reader = new FileReader();
                    
                            reader.addEventListener("load", function () {
                                preview.src = reader.result;
                            }, false);
                    
                            if (file) {
                                reader.readAsDataURL(file);
                            }
                        }
                    </script>
                </div> 
            </div>
        </div>
    </div>
    
</x-app-layout>
