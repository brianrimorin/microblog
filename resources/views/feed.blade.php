<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Feed') }}
        </h2>
    </x-slot>

    @include('layouts.alert')

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                {{-- Search --}}
                <div class="p-6 bg-white border-b border-gray-200">
                    @include('layouts.search')
                </div>

                <div class="p-6 bg-white border-b border-gray-200">
                    @include('layouts.new-post')
                </div>

                @forelse ($posts as $post)
                    <div class="p-6 bg-white border-b border-gray-200">
                        @include('layouts.post')
                        @include('layouts.post-buttons')
                    </div>
                @empty
                    No posts available.
                @endforelse

                {{ $posts->links() }}
            </div>
        </div>
    </div>
</x-app-layout>
